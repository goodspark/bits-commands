import time
from typing import ClassVar, List, Dict, Type

import attr
import keyboard
import mouse
import yaml


@attr.s(frozen=True, slots=True, auto_attribs=True)
class Action:
    name: ClassVar[str]
    args: List[str]

    def _run(self):
        raise NotImplementedError

    def run(self):
        print(f"running: {self.name}, args: {self.args}")
        self._run()


class Click(Action):
    name = "click"
    _BUTTONS = {"left", "right", "middlze"}

    def _run(self):
        button = self.args[0].lower()
        time_s = None
        if len(self.args) >= 2:
            time_s = float(self.args[1])
        if button not in self._BUTTONS:
            print(f"unknown mouse button: {button}")
        else:
            if time_s is None:
                mouse.click(button)
            else:
                mouse.press(button)
                time.sleep(time_s)
                mouse.release(button)


class ButtonPress(Action):
    name = "button press"

    def _run(self):
        keyboard.send(self.args[0])


class Wait(Action):
    name = "wait"

    def _run(self):
        time_s = float(self.args[0])
        time.sleep(time_s)


_ACTIONS: Dict[str, Type[Action]] = {
    "click": Click,
    "wait": Wait,
    "press": ButtonPress,
}
_COMMANDS = {}


@attr.s(frozen=True, slots=True, auto_attribs=True)
class Command:
    name: str
    actions: List[Action]

    @classmethod
    def from_config(cls, config: Dict) -> "Command":
        actions = []
        for action_cfg in config["actions"]:
            action_type = action_cfg["type"]
            action_args = action_cfg["args"]
            action = _ACTIONS[action_type](action_args)
            actions.append(action)

        c = Command(config["name"], actions)
        return c

    def run(self):
        print(f"command: {self.name}")
        for action in self.actions:
            action.run()


def parse_config():
    with open('config.yml') as f:
        cfg = yaml.load(f)
        for command_cfg in cfg["commands"]:
            c = Command.from_config(command_cfg)
            _COMMANDS[c.name] = c


def bits_command(command: str):
    if command not in _COMMANDS:
        print(f"unknown command: {command}")
        return
    _COMMANDS[command].run()


if __name__ == "__main__":
    parse_config()
    bits_command("nade out")
    bits_command("prone zone")
    bits_command("empty clip")
    bits_command("something weird")
